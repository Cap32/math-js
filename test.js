var mathjs = require('./math')
var assert = require('assert')

assert(mathjs.add(30, 2) === 32)
console.log('pass add()')

assert(mathjs.avg(30, 34) === 32)
console.log('pass avg()')

console.log('done')

